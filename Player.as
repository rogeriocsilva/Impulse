﻿package{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Player extends MovieClip{
		
		var leftKeyDown:Boolean = false;
		var upKeyDown:Boolean = false;
		var rightKeyDown:Boolean = false;
		var downKeyDown:Boolean = false;
		var spaceKeyDown:Boolean = false;
		
		var vidas:Number = 0;
		var score:Number =0;
		
		var floor:int;
		
		
		var gravidade:Number = 0;
		var gasolina:Number = 100;
		var mainSpeed:int = 3;
		
		var flying:Boolean = false;
		var jumping:Boolean = false;
		
		
		public function Player():void{
			floor = 480;
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		private function onAddedToStage(e:Event):void{
			
			this.gotoAndStop("still");
			
			stage.addEventListener(Event.ENTER_FRAME,onenter);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, checkKeysDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, checkKeysUp);
		}
		
		function checkKeysDown(event:KeyboardEvent):void{
			if(event.keyCode == 37 || event.keyCode == 65){
				leftKeyDown = true;
			}
			if(event.keyCode == 38 || event.keyCode == 87){
				upKeyDown = true;
			}
			if(event.keyCode == 39 || event.keyCode == 68){
				rightKeyDown = true;
			}
			if(event.keyCode == 40 || event.keyCode == 83){
				downKeyDown = true;
			}
			if(event.keyCode == 32){
				
				spaceKeyDown = true;
			}
		}
		
		function checkKeysUp(event:KeyboardEvent):void{
			if(event.keyCode == 37 || event.keyCode == 65){
				leftKeyDown = false;
			}
			if(event.keyCode == 38 || event.keyCode == 87){
				upKeyDown = false;
			}
			if(event.keyCode == 39 || event.keyCode == 68){
				rightKeyDown = false;
			}
			if(event.keyCode == 40 || event.keyCode == 83){
				downKeyDown = false;
			}
			if(event.keyCode == 32){
				spaceKeyDown = false;
				flying = false;
			}
		}
		
		function onenter(e:Event):void{
			if(rightKeyDown){
			   this.x+=mainSpeed;
			   this.scaleX = 1;
			   this.gotoAndStop('walking');
			}
			if(leftKeyDown){
			   this.x-=mainSpeed;
			   this.scaleX = -1;
			   this.gotoAndStop('walking');
			}
			if(upKeyDown && !jumping){
				jumping = true;
				this.gravidade = -15;
				
			}
			if(jumping == true){
				this.gotoAndStop('jumping');
				}
			
			if(!rightKeyDown && !leftKeyDown && !jumping){
				this.gotoAndStop("still");
			}
			if(spaceKeyDown && gasolina>0){
				flying=true;
				jumping=true;
				this.gravidade-=0.25;
				gasolina-=2;
				
				this.gotoAndStop("flying");
				
			}
			
			if(gasolina == 0){
				flying = false;
			}
			adjust();
			
		}
		
		function adjust():void{
			
			this.y+=gravidade;
			
			if(!flying){
				if(this.y+this.height/2 < floor){
					gravidade++;
				}
				else{
					gravidade = 0;
					this.y = floor - this.height/2;
					jumping = false;
					flying = false;
				}
			}
			
			if(this.x -this.width*0.3 <0){
				this.x = this.width*0.25;
			}
			if(this.x +this.width*0.3 > 640 ){
				this.x = 640 - this.width*0.25;
			}
			if(this.y +this.height*0.3 < 0 ){
				gravidade =0;
				this.y = 0 + this.height*0.2;
			}
			
			if(this.y +this.height/2 > floor){
				gravidade = 0;
				this.y = floor - this.height*0.2;
			}

			
		}
		
		
	}//fim da classe
	
}
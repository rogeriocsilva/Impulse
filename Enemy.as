﻿package  {
	
	import flash.display.MovieClip;
	
	public class Enemy extends MovieClip{
			
		var vida:int;
		var randMin:int;
		var randMax:int;
		var direcao:int; //1 positiva -1 negativa
 
       	public function Enemy(X:int,Y:int,W:int,H:int,v:int) {
       		this.x=X;
			this.y=Y;
			this.width=W;
			this.height=H;
			this.vida = v;
			this.randMin = Math.ceil(Math.random()*320);
			this.randMax = 320+Math.ceil((Math.random()*320));
			if(Math.random() >=0.5){
				this.direcao = 1;
			}
			else{
				this.direcao = -1;
			}
			this.scaleX *=this.direcao;
			
		}

	}
	
}

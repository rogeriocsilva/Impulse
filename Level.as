﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	import flash.display.Stage;
	import flash.events.EventDispatcher;
	


	public class Level extends MovieClip{


		var scoreText:TextField = new TextField();
		var fuelText:TextField = new TextField();
		var textStyle:TextFormat = new TextFormat("Comic Sans",15,"#FFFFFF");
		
		var ganhou:Boolean = false;
		var morreu:Boolean = false;
		var nivel:int = 0;
		var plat_speed:int =0;

		var winning_blocks:Array = new Array();
		var enemies:Array = new Array();
		var blocks:Array = new Array();
		var gas:Array = new Array();
		var coins:Array = new Array();
		
		var mySound:GunSound = new GunSound();
		
		var pickSound:PowerUp = new PowerUp();
		
		var bullets:Array = new Array();

		var player:Player;
		var map:Array;

		public function Level() {
			
		}
		
		 protected function init(e:Event):void {
			scoreText.x = 10;
			scoreText.y = 20;
			fuelText.x = 10;
			fuelText.y = 35;
			scoreText.defaultTextFormat = textStyle;
			scoreText.textColor = 0xFFFFFF;
			fuelText.defaultTextFormat = textStyle;
			fuelText.textColor = 0xFFFFFF;
			addChild(scoreText);
			addChild(fuelText);
            removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, shoot);
        }
		

		function shoot(e:Event):void{
			var angle:Number = Math.atan2(mouseY - player.y, mouseX - player.x);
			var speed:Number = 5;
			var bullet:Bullet = new Bullet(angle,speed);
			bullets.push(bullet);
			bullet.x = player.x;
			bullet.y = player.y;
			addChild(bullet);
			mySound.play();
			this.addEventListener(Event.ENTER_FRAME, moveb);
		}

		function moveb(e:Event):void {
			if(bullets.length > 0) {
				for(var i:Number = 0; i < bullets.length; i++) {
					if(bullets[i] != null)
						bullets[i].moveBullet();
				}
			}
		}
		


		function checkColisions():void{
			var number:Number = hitTest(player, blocks);
           	if (number!=-1){
            	player.floor = blocks[number].getY() -blocks[number].getHeight()/2 +5;
               }
			else{
				player.floor = 480;
			}
			for(var i:int = 0;i<gas.length;i++){
				if(gas[i]!=null){
					if(player.hitTestObject(gas[i])){
						player.gasolina+=50;
						pickSound.play();
						if(gas[i].parent != null)
							gas[i].parent.removeChild(gas[i]);
							gas[i]=null;
					}
				}
			}
			for(var j:int = 0;j<coins.length;j++){
				if(coins[j]!=null){
					if(player.hitTestObject(coins[j])){
						player.score++;
						pickSound.play();
						if(coins[j].parent != null)
							coins[j].parent.removeChild(coins[j]);
							coins[j]=null;
					}
				}
			}
			for(var l:int = 0;l<enemies.length;l++){
				if(enemies[l]!=null){
					if(player.hitTestObject(enemies[l])){
						player.vidas--;
						morreu = true;
					}
				}
			}
			for(var m:int = 0;m<bullets.length;m++){
				for(var n:int = 0; n < enemies.length; n++) {
					if(bullets[m]!=null && enemies[n]!= null){
						if(bullets[m].hitTestObject(enemies[n])){
							enemies[n].vida--;
							if(enemies[n].vida <=0){
								bullets[m].parent.removeChild(bullets[m]);
								enemies[n].parent.removeChild(enemies[n]);
								enemies[n] = null;
								bullets[m] = null;
								player.score+=2;
							}
							else{
								bullets[m].parent.removeChild(bullets[m]);
								bullets[m] = null;
							}
						}
					}
				}
			}
			for(var o:int = 0;o<winning_blocks.length;o++){
				if(player.hitTestObject(winning_blocks[o])){
					this.ganhou=true;
				}
			}

		}

		

		public function hitTest(ObjA:MovieClip, ObjB:Array): Number{
			for(var i:int=0;i<blocks.length;i++){
        		if((ObjA.x  >ObjB[i].x-ObjB[i].width && ObjA.x - ObjA.width*0.3 <ObjB[i].x+ObjB[i].width/2 && ObjA.y +ObjA.height*0.35< ObjB[i].y - ObjB[i].height*0.5)){
					return i;
				}

			}
			return -1;
		}

		public function plataformas():void{
			for(var i:int=0;i<blocks.length;i++){
				blocks[i].y-=(map.length-15)*32;
			}
			for(var j:int=0;j<gas.length;j++){
				gas[j].y-=(map.length-15)*32;
			}
			for(var k:int=0;k<coins.length;k++){
				coins[k].y-=(map.length-15)*32;
			}
			for(var l:int=0;l<enemies.length;l++){
				enemies[l].y-=(map.length-15)*32;
			}
			for(var m:int=0;m<winning_blocks.length;m++){
				winning_blocks[m].y-=(map.length-15)*32;
			}

		}
		public function plataformas2():void{
			for(var i:int=0;i<blocks.length;i++){
				blocks[i].y+=plat_speed;
			}
			for(var j:int=0;j<gas.length;j++){
				if(gas[j]!=null){
					gas[j].y+=plat_speed;
				}
			}
			for(var k:int=0;k<coins.length;k++){
				if(coins[k]!=null){
					coins[k].y+=plat_speed;
				}
			}
			for(var l:int=0;l<enemies.length;l++){
				if(enemies[l]!=null){
					enemies[l].y+=plat_speed;
					enemies[l].x+=enemies[l].direcao;
					if(enemies[l].direcao == -1 && enemies[l].x<enemies[l].randMin + enemies[l].width*0.35 ){
						enemies[l].direcao =1;
					}
					if(enemies[l].direcao == 1 && enemies[l].x>enemies[l].randMax + enemies[l].width*0.35 ){
						enemies[l].direcao = -1;
					}


				}
			}
			for(var m:int=0;m<winning_blocks.length;m++){
				winning_blocks[m].y+=plat_speed;
			}
			
		}
		function loop(e:Event):void{
			scoreText.text = "Score: "+player.score;
			fuelText.text = "Gasolina: "+ player.gasolina; 
			checkColisions();			
			if(player.y >= 455){
				player.vidas--;
				morreu = true;
			}
			if(morreu && stage){
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, shoot);
			}
			
			plataformas2();
			
			for(var i:Number = 0; i < bullets.lenght; i++) {
				bullets[i].moveBullet;
			}
				
		}
		
	}

}

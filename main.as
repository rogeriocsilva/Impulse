﻿package  {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.SimpleButton;
	import flash.net.SharedObject;	
	import flash.text.*; 
	import flash.media.Sound;
	import flash.media.SoundChannel;

	
	public class main extends MovieClip{
		
		var level:Level;
		var score:SharedObject = SharedObject.getLocal("Scores");
		var textStyle:TextFormat = new TextFormat("Arial",25,"#FFFFFF");
		var myMusic:MusicBG = new MusicBG();
		var myChannel:SoundChannel = new SoundChannel();
		


		public function main() {
			if (this.score.data.highScore1 == null){
        		this.score.data.highScore1 = 0;
			}
			
			if (this.score.data.highScore2 == null){
        		this.score.data.highScore2 = 0;
			}
			
			if (this.score.data.highScore3 == null){
        		this.score.data.highScore3 = 0;
			}

			this.mostraMenu();
			stage.addEventListener(Event.ENTER_FRAME,loop);
			
		}
		public function loop(e:Event):void{
			if(level!=null) {
				var vidas_restantes:int = level.player.vidas;
				var current_score:int = level.player.score;
				var current_level:int = level.nivel;
				if(level.ganhou){
					if(current_level == 3){
						verificaScore(level.player.score);
						menuWin();
					}
					else{
						menuNextLevel(vidas_restantes,current_score,current_level+1);
					}
				}
				if(level.morreu){
					if(vidas_restantes >0){
						if(level.parent){
							menuTryAgain(vidas_restantes,current_score,level.nivel);
						}
					}
					else {
						if(level.parent){
							verificaScore(level.player.score);
							menuLost();
						}
					}
				}
			}
		}
		
		public function verificaScore(gameScore:int):void {
			if (this.score.data.highScore1 == null){
        		this.score.data.highScore1 = 0;
			}
			
			if (this.score.data.highScore2 == null){
        		this.score.data.highScore2 = 0;
			}
			
			if (this.score.data.highScore3 == null){
        		this.score.data.highScore3 = 0;
			}
			
			var aux1:int;
			var aux2:int;
			
			if (gameScore > this.score.data.highScore1) {
				aux1 = this.score.data.highScore1;
				aux2 = this.score.data.highScore2;
				this.score.data.highScore1 = gameScore;
				this.score.data.highScore2 = aux1;
				this.score.data.highScore3 = aux2;
			} else if (gameScore > this.score.data.highScore2) {
				
				aux1 = this.score.data.highScore2;
				this.score.data.highScore2 = gameScore;
				this.score.data.highScore3 = aux1;

			} else if (gameScore > this.score.data.highScore3) {
				this.score.data.highScore3 = gameScore;
			}
		}
		
		public function newLevel(vidas_restantes:int,current_score:int,current_level:int):void{
			clearStage();
			if(current_level == 1){
				level = new Level1(vidas_restantes,current_score);
			}
			if(current_level == 2){
				level = new Level2(vidas_restantes,current_score);
			}
			else if(current_level == 3){
				level = new Level3(vidas_restantes,current_score);
			}
			
			addChild(level);
		}
		
		public function mostraMenu():void {
			clearStage();
			
			var bg:backPrincipal = new backPrincipal();
			addChild(bg);
			myChannel.stop();
			
			var iniciar:BotaoIniciar = new BotaoIniciar();
			var creditos:BotaoCreditos = new BotaoCreditos();
			var ranking:BotaoRanking = new BotaoRanking();
			var instrucoes:BotaoInstrucoes= new BotaoInstrucoes();
			
			addChild(iniciar);
			addChild(creditos);
			addChild(ranking);
			addChild(instrucoes);
			ranking.x = 320;
			ranking.y = 305;
			instrucoes.x = 320;
			instrucoes.y = 260;
			creditos.x = 320;
			creditos.y = 350;
			iniciar.x = 320;
			iniciar.y = 200;
			
			iniciar.addEventListener(MouseEvent.MOUSE_UP, iniciarJogo);
			creditos.addEventListener(MouseEvent.MOUSE_UP, mostraCreditos);
			ranking.addEventListener(MouseEvent.MOUSE_UP, mostraRanking);
			instrucoes.addEventListener(MouseEvent.MOUSE_UP, mostraInstrucoes);
			
			
		}
		
		public function iniciarJogo(e:Event):void{
			clearStage();
			myChannel = myMusic.play(0,10);
			this.level = new Level1(3,0);
			addChild(level);
			
		}
		
		public function clearStage():void{
			while(numChildren >0){
				removeChildAt(0);
			}
		}
		public function mostraCreditos(e:Event):void{
			clearStage();
			var voltar:BotaoVoltar = new BotaoVoltar();
			
			var bg:CreditosBG = new CreditosBG();
			addChild(bg);
			addChild(voltar);
			voltar.x = 320;
			voltar.y = 450;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP, function() {mostraMenu()});
		}
		public function menuTryAgain(vidas_restantes:int,current_score:int,current_level:int):void{
			clearStage();
			
			var bg:TryAgainBG = new TryAgainBG();
			addChild(bg);
			var voltar:TryAgainBT = new TryAgainBT();
			addChild(voltar);
			voltar.x = 320;
			voltar.y = 450;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP, function() {newLevel(vidas_restantes,current_score,current_level)});
			
		}
		
		public function menuLost():void{
			clearStage();
			var bg:LostBG = new LostBG();
			addChild(bg);
			var voltar:BotaoVoltar = new BotaoVoltar();
			addChild(voltar);
			var restart:BotaoRestart = new BotaoRestart();
			addChild(restart);
			
			voltar.x = 320;
			voltar.y = 400;
			restart.x = 320;
			restart.y = 350;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP,function() {mostraMenu()});
			restart.addEventListener(MouseEvent.MOUSE_UP, iniciarJogo);
		}
		
		public function menuNextLevel(vidas_restantes:int,current_score:int,current_level:int):void{
			clearStage();
			var bg:NextLevelBG = new NextLevelBG();
			var continueBT:NextLevelBT = new NextLevelBT();
			addChild(bg);
			addChild(continueBT);
			continueBT.x = 320;
			continueBT.y = 400;
			
			continueBT.addEventListener(MouseEvent.MOUSE_UP, function() {newLevel(vidas_restantes,current_score,current_level)});
		}
		public function mostraInstrucoes(e:Event):void{
			clearStage();
			var voltar:BotaoVoltar = new BotaoVoltar();
			
			var bg:InstructionsBG = new InstructionsBG();
			addChild(bg);
			addChild(voltar);
			voltar.x = 320;
			voltar.y = 450;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP, function() {mostraMenu()});
		}
		public function mostraRanking(e:Event):void{
			clearStage();
			var voltar:BotaoVoltar = new BotaoVoltar();
			var scoreText1:TextField = new TextField();
			var scoreText2:TextField = new TextField();
			var scoreText3:TextField = new TextField();
			
			scoreText1.defaultTextFormat = textStyle;
			scoreText2.defaultTextFormat = textStyle;
			scoreText3.defaultTextFormat = textStyle;
			
			//scoreText1.border = true;
			scoreText1.x = 319;
			scoreText1.y = 200;
			scoreText1.text = this.score.data.highScore1;
			
			scoreText2.x = 320;
			scoreText2.y = 250;
			scoreText2.text = this.score.data.highScore2;
			
			scoreText3.x = 320;
			scoreText3.y = 300;
			scoreText3.text = this.score.data.highScore3;
			
			var bg:RankingsBG = new RankingsBG();
			
			addChild(bg);
			addChild(voltar);
						
			addChild(scoreText1);
			addChild(scoreText2);
			addChild(scoreText3);
			voltar.x = 320;
			voltar.y = 450;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP, function() {mostraMenu()});
		}
		
		public function menuWin():void{
			clearStage();
			this.level = null;
			var voltar:BotaoVoltar = new BotaoVoltar();			
			var bg:menuWinBG = new menuWinBG();
			addChild(bg);
			addChild(voltar);
			voltar.x = 320;
			voltar.y = 450;
			
			voltar.addEventListener(MouseEvent.MOUSE_UP, function() {mostraMenu()});
		}
	}
}
		
	